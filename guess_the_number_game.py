#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 22:40:07 2018

@author: estebian
"""

from random import randint

		
def getPlayerName():
    while True:
        print('-' * 20)
        print('(/•ิ_•ิ) - Welcome to guess number game. What is your name?')
        print('-' * 20)
        name = input('>>> ')
        if name == '':
            print('(⊙…⊙) - Please, write your name and press ENTER to continue...')
            print('-' * 20)
        else:
            print('[-ิ_•ิ] - Nice to meet you ' + name + '!, I am ' +  bin(randint(64,128))[2:] + 'computer.' )
            print('-' * 20)
            return name
            break

def playerVsCpu():
    
    cpuNumber = randint(1, 20)    
    for oportunities in range(1, 7):
        
        while True:
            try:
                print('-' * 20)
                print('Please ' + playerName + ', Type a number and press ENTER:')
                print('-' * 20)        
                playerNumber = int(input('>> '))
                break
            except ValueError:
                print('Nope. Wrong value! Please type an integer number...')
            
        
        if playerNumber < cpuNumber:
            print('Your guess is too low.')
        elif playerNumber > cpuNumber:
            print('Your guess is to high.')
        else: break
    
    if playerNumber == cpuNumber:
        print('\(•ิ_•ิ)/: GOOD JOB ' + playerName + '! You guessed the number in ' + str(oportunities) + ' guesses!')
    else:
        print('(-’๏_๏’-): SORRY ' + playerName + '. The number I was thinking of was ' + str(cpuNumber))

playerName = getPlayerName()

print('...Well ' + playerName + ', (︶︹︺) I am thinking of a number between 1 and 20.')
print('::: (/•ิ_•ิ) - You have six chances to guess de correct number :::')
print('-' * 20)
print('[-ิ_•ิ] - Come on let`s to play!')
print('-' * 20)

playerVsCpu()


        
        
        
    
   
    


